package com.demo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.demo.model.Wish;
import java.util.ArrayList;

@Repository
public interface WishRepository extends CrudRepository<Wish, Long> {

    ArrayList<Wish> findByUsernameOrderById(String pengguna);

}

