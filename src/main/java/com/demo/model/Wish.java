package com.demo.model;

import org.springframework.beans.factory.annotation.Value;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Wish {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Temporal(TemporalType.DATE)
    private Date wishDate;

    @Column(nullable = false, unique = false)
    private String title;

    @Column(nullable = true, unique = false)
    private String category;

    @Column(nullable = false, unique = false)
    private long price;

    @Column(nullable = false, unique = false)
    @Value("${user:}")
    private String username;

    public Wish(String title, String username, Date wishDate, String category, long price) {
        this.title = title;
        this.username = username;
        this.wishDate = wishDate;
        this.category = category;
        this.price = price;
    }

    public Wish() {}

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public long getPrice() {
        return this.price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getUser() {
        return this.username;
    }

    public void setUser(String username) {
        this.username = username;
    }

    public Date getWishDate() {
        return this.wishDate;
    }

    public void setWishDate(Date date) {
        this.wishDate = date;
    }
}
