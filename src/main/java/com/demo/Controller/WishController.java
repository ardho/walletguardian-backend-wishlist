package com.demo.Controller;

import com.demo.model.Wish;
import com.demo.repository.WishRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@CrossOrigin(origins="https://wallet-guardian.herokuapp.com")
@RestController
public class WishController {

    @Autowired
    WishRepository wishRepository;

    @RequestMapping(value = "add-wish", method = RequestMethod.POST)
    @ResponseBody
    public List<Wish> postWishlist(@RequestParam String title,
                                                     @RequestParam String category,
                                                     @RequestParam String user,
                                                     @RequestParam long price) {

        Date wishDate = java.sql.Date.valueOf(LocalDate.now());
        Wish wish = new Wish(title, user, wishDate, category, price);
        wishRepository.save(wish);
        return getWishlist(user);
    }

    @RequestMapping(value = "wishlist/{pengguna}", method = RequestMethod.GET)
    @ResponseBody
    public List<Wish> wishlist(@PathVariable String pengguna) {
        return getWishlist(pengguna);
    }

    public List<Wish> getWishlist(String user) {
        List<Wish> wishlist = new ArrayList<Wish>();
        wishlist.addAll(wishRepository.findByUsernameOrderById(user));
        return wishlist;
    }

}
