package com.demo;

import com.demo.DemoApplication;
import com.demo.model.Wish;
import org.junit.Assert;
import org.junit.Test;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;
import java.util.Date;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = Wish.class, properties = "spring.profiles.active=test")
public class WishTest {

  @Autowired
  private MockMvc wish;

  @Test
  public void wishModel() throws Exception {
    String title = "HP";
    String category = "Elektronik";
    Date wishDate = java.sql.Date.valueOf(LocalDate.now());
    String user = "Aku";
    int price = 20000;
    Wish w1 = new Wish();
    w1 = new Wish(title, category, wishDate, user, price);

    w1.setTitle("HP");
    w1.setCategory("Elektronik");
    w1.setWishDate(wishDate);
    w1.setUser("Aku");
    w1.setPrice(2000);
    Assert.assertEquals("HP", w1.getTitle());
    Assert.assertEquals("Elektronik", w1.getCategory());
    Assert.assertEquals(wishDate, w1.getWishDate());
    Assert.assertEquals("Aku", w1.getUser());
    Assert.assertEquals(2000, w1.getPrice());
  }
}
