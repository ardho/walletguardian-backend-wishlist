package com.demo;

import com.demo.Controller.WishController;
import com.demo.repository.WishRepository;
import org.junit.Test;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = WishController.class, properties = "spring.profiles.active=test")
public class WishControllerTest {

  @MockBean
  private WishRepository pengeluaranRepository;

  @Autowired
  private MockMvc wishController;

  @Autowired
  private MockMvc mockMvc;

  @Test
  public void testIndex() throws Exception {
    wishController.perform(MockMvcRequestBuilders.get("/"))
        .andExpect(status().isNotFound());
  }

  @Test
  public void testNoData() throws Exception {
    wishController.perform(MockMvcRequestBuilders.get("/wishlist/Aku"))
        .andExpect(content().json("[]"));
  }

  @Test
  public void testGetWishUser() throws Exception {
    wishController.perform(MockMvcRequestBuilders.get("/wishlist/ini-username"))
        .andExpect(status().isOk());
  }

  @Test
  public void testAddWish() throws Exception {
    String title = "HP";
    String category = "Elektronik";
    String user = "Aku";
    int price = 20000;
    String urlTemplate = "/add-wish?"
        + "title="+ title
        + "&category=" + category
        + "&user=" + user
        + "&price=" + price;
    wishController.perform(MockMvcRequestBuilders.post(urlTemplate))
        .andExpect(status().isOk());
  }

}
