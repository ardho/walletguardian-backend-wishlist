# Wallet Guardian
Kelompok: `C-6`

Gitlab | Heroku
-
- Frontend: [gitlab](https://gitlab.com/janitraa/frontendWG) | [heroku](http://wallet-guardian.herokuapp.com)
- Backend (transaksi): [gitlab](https://gitlab.com/janitraa/wallet-guardian) | [heroku](http://w-g-backend.herokuapp.com)
- Backend (wishlist): [gitlab](https://gitlab.com/rd0/walletguardian-backend-wishlist) | [heroku](http://w-g-wishlist.herokuapp.com)
